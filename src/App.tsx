import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import Parent from './Parent'
import Child from './Child'

function App() {

  return (
    <Parent>
      <div>
        <Child/>
        <Child/>
        </div>
        <div>
          <div>
            <div>
              <Child />
            </div>
            </div>
        </div>

      <Child/>
      <Child/>
      <Child/>
    </Parent>
  )
}

export default App
