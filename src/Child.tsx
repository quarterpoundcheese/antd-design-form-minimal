
const Child = ({found = false}: {found?: boolean}) => {

    return (
        <div>
            {
                found ? 'Found' : 'Not found'
            }
        </div>

    )
}

export default Child