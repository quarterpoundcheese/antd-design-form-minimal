import { renderer } from "./constants"


export default function Parent({children}: {children: JSX.Element[]}) {
    const f = renderer(children, () => {});

    return (
        <>
            {f}
        </>
    )
}