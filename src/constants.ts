import { cloneElement } from "react";

export const renderer = (s: JSX.Element[], reg: () => void): JSX.Element[] => {
    return s.map((t) => {
        if(t.props.children) {
            if(Array.isArray(t.props.children)) {
                return renderer(t.props.children, reg)
            }
            if(typeof t.props.children === 'object') {
                return renderer([t.props.children], reg)
            }
        }

        if(t.type.name !== 'Child') return t; 

        return cloneElement(t, {
            found: true
        })
    }).flat(10)
}